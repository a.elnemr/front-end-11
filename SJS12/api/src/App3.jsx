import React from 'react';
import axios from 'axios';
import {Link, Route, Switch} from 'react-router-dom';

const BASE_URL = 'http://dev.uguworld.com/api';

class App3 extends React.Component {

    state = {
        appData: null
    }

    getAppData = () => {
        axios.get(BASE_URL + '/1.0/app-data')
            .then(res => this.setState({appData: res.data}))
    }

    componentDidMount() {
        this.getAppData();
    }

    render() {
        console.log(this.state.appData && this.state.appData.response.data.post_categories)
        return (
            <div>
                App
            </div>
        );
    }
}

export default App3;