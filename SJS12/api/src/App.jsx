import React from 'react';
import axios from 'axios';
import {Link, Route, Switch} from 'react-router-dom';

const BASE_URL = 'http://dev.uguworld.com/api';

class App extends React.Component {

    state = {
        user: null,
        posts: [],
        newPost: {
            type: 'TEXT',
            content: 'Added New Post'
        }
    };

    authHeader = () => {
        return {
            headers: {
                Authorization : `Bearer ${localStorage.getItem('token')}`
            }
        }
    }

    login = () => {
        const userData = {
            email: 'user-15@ugu.com',
            password: 'S1234s12',
            fcm_token: '7zIh8sa-NFdeFfAwoNLW_xF-1BsQktyaHzRQkQZt02Ltfhhl89fHq080s0Qxg4zkYSg4-kHOmX4epVdmj_58qIC9RCiXRYc5yKLM4ZdHDNYUjcini96xTVfAaDH5EGIYU9t',
            device_id: 'ytS9KIBuiYUo',
        }

        axios.post(BASE_URL + '/1.0/auth/login', userData)
            .then(res => {
                this.setState({user: res.data.response.data.user});
                localStorage.setItem('token', res.data.response.data.token);
            }, error => {
               console.log(error)
            });
    }

    getFeed = () => {
        axios.get(BASE_URL + '/1.0/posts/feed-filters', this.authHeader())
            .then(res => {
                this.setState({posts: res.data.response.data})
            })
    }


    userProfile = () => {
        axios.get(BASE_URL + '/1.0/user', this.authHeader())
            .then(res => this.setState({user: res.data.response.data}));
    }

    addNewPost = () => {
        axios.post(BASE_URL + '/1.0/posts', this.state.newPost, this.authHeader())
            .then(res => console.log(res));
    }

    logout = () => {
        localStorage.removeItem('token');
    }

    render() {
        console.log(this.state.posts);
        return (
            <div>
                <button onClick={this.login}>Login</button>
                <button onClick={this.getFeed}>Get Posts</button>
                <button onClick={this.userProfile}>Get User Profile</button>
                <button onClick={this.addNewPost}>AddNew Post</button>
                <button onClick={this.logout}>Logout</button>
                {localStorage.getItem('token')}
            </div>
        );
    }
}

export default App;