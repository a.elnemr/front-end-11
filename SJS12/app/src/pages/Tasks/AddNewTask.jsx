import React from 'react';
import Tasks from "../../components/Tasks";

export const AddNewTask = props => (
  <div className="new-task">
    <Tasks.Form newTaskTitle={props.newTaskTitle}
                newTaskTitleOnChange={props.newTaskTitleOnChange}
                addSubTaskOnClickHandle={props.addSubTaskOnClickHandle}
                subTaskInputs={props.subTaskInputs}
                subTaskInputOnChange={props.subTaskInputOnChange}
                removeSubTaskOnClickHandle={props.removeSubTaskOnClickHandle}
                addNewTaskOnSubmit={props.addNewTaskOnSubmit}/>
  </div>
);

export default AddNewTask;