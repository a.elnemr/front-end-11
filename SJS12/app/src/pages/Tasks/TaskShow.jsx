import React from 'react';
import Tasks from "../../components/Tasks";

export const TaskShow = props => {

  const {id} = props.match.params, // id = props.match.params.id, name = props.match.params.name
    task = props.findTask(id);

  return (
    <div>
      {task ?
        <div>
          <h1>{task && task.title}</h1>
          <ul>
            {task && task.subTasks && task.subTasks.map(subTask => (
              <li key={subTask.id}><span>{subTask.title}</span>
                <button className="btn btn-success">Done</button>
              </li>
            ))}
          </ul>
        </div>
        : <h1>404 Page Not Found</h1>}
    </div>
  )
};

export default TaskShow;