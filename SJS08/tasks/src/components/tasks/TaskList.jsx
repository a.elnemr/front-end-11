import React from "react";
import TaskItem from "./TaskItem";

export const TaskList = () => (
    <ul className="list-group">
        <TaskItem/>
        <TaskItem/>
        <TaskItem/>
        <TaskItem/>
        <TaskItem/>
        <TaskItem/>
        <TaskItem/>
        <TaskItem/>
    </ul>
);

export default TaskList;
