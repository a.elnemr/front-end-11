import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App'

// App Component
// const App = function () {
//     const name = 'Test name';
//     return (
//         <div>
//             <h1>React App</h1>
//             <p>{name} JSX</p>
//         </div>
//     );
// };
// Render JSX to DOM

ReactDOM.render(
    <App />,
    document.getElementById('app')
);
