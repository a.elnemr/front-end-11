const btnItems = document.getElementsByClassName('item'),
    clickHandle = function (e) {
        document.getElementById('content').
        innerHTML = e.target.dataset.item;
    }
;
/**
 * {
 *     name: 'Mohamed',
 *     item: '1',
 * }
 */
// Array (HTMLCollection)
// btnItems.addEventListener('click', clickHandle);
// element
// btnItems[0].addEventListener('click', clickHandle);
// btnItems[1].addEventListener('click', clickHandle);
// btnItems[2].addEventListener('click', clickHandle);
// btnItems[3].addEventListener('click', clickHandle);
// btnItems[4].addEventListener('click', clickHandle);

// btnItems
// console.log(btnItems.length);
// loop
// init value; condition; increment
// debugger;
for (let i = 0; i < btnItems.length; i++) {
    btnItems[i].addEventListener('click', clickHandle);
}
