// Variables
const $ = document,
    btnItems = $.querySelectorAll('.btn-item'),
    pContents = $.querySelectorAll('p.content')
;

// actions (functions)
const itemClickHandle = function (e) {
    const item = e.target.dataset.item,
        content = $.querySelector(`#content-${item}`)// '#content-' + item
        ;

    if (e.target.classList.contains('active')) {
        // resItems();
        e.target.classList.remove('active');
        content.classList.add('hidden');
        return;
    }

    // resItems();
    e.target.classList.add('active');
    content.classList.remove('hidden');

};

// Events
for (let i = 0; i < btnItems.length; i++) {
    btnItems[i].addEventListener('click', itemClickHandle);
}
