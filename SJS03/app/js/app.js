(function() {
    "use strict";
    //  number (int, float) value, value type
    let age = 30,
        // string value, reference type
        gender = 'male';

    // object , reference type
    const student = {
        id : 1,
        age: 30,
        name: 'Ali Mohamed',
        subjects: ['JS', 'HTML', 'CSS'],
        car: {
            color: 'red',
            cc: 2000,
            maxSpeed: 180
        }
    };

    // array , reference type
    //                   0     ,  1      , 2
    const listItems = [
            1,
            25,
            'Sara mohamed',
            {id: 99, age: 50},
            ['App', 'Orange']
        ];
})();


