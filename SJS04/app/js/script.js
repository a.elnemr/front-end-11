const _ = document,
    btnResult = _.getElementById('btn-result');

const btnHandle = function (e) {
    const number1 = _.getElementById('number1').value,
        number2 = _.getElementById('number2').value,
        operation = _.getElementById('operation').value
    ;
    // debugger;
    let result = null;

    // condition
    // if (operation == '+') { // block scope
    //     result = Number(number1) + Number(number2);
    //     // result = parseFloat(number1) + parseFloat(number2);
    // } else if (operation == '-') {
    //     result = Number(number1) - Number(number2);
    // } else if (operation == '*') {
    //     result = Number(number1) * Number(number2);
    // } else if (operation == '/') {
    //     result = Number(number1) / Number(number2);
    // } else {
    //     result = 'Wrong Operation';
    // }
    // debugger;
    // expression
    switch (operation) {
        case '+': result = Number(number1) + Number(number2); break;
        case '-': result = Number(number1) - Number(number2); break;
        case '*': result = Number(number1) * Number(number2); break;
        case '/':
            if (number2 == 0) {
                result = 'unaccepted';
                break;
            }

            result = Number(number1) / Number(number2);
            break;
        default: result = 'Wrong Operation';
    }

    // const value = 7;
    // switch (true) {
    //     case (value > 1 && value < 5): console.log('from 1 to 5'); break;// false
    //     case (value > 6 && value < 10): console.log('from 6 to 10'); break;// true
    //     case (value > 11 && value < 30): console.log('from 6 to 10'); break;// false
    //     default: console.log('Else');
    // }


    // const display = result ? result : 'Wrong Operation';// result ?? 'Wrong Operation'

    console.log(result);
};

btnResult.addEventListener('click', btnHandle);

function sum(start, end = 9) {
    return start + end;
}

console.log(sum(2, 8));
