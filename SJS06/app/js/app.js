"use strict";
$(function () { // page is ready

    // functions
    function printAllPosts(posts) {
        if (posts.length === 0) {
            $('#all-posts').html(`
                <div class="col-12 text-center">
                    <p>No Posts !</p>
                </div>
            `);
            return;
        }

        for (let i = 0; i < posts.length; i++) {
            // $('#all-posts').append('<h1>' + posts[i].title + '</h1>');
            const post = `<div class="col-md-4 mb-5">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">${posts[i].title.substr( 1, 20).toLocaleUpperCase()}...</h5>
                            <p class="card-text">
                            ${posts[i].body.substr(1, 50)}...
                            </p>
                            <a href="https://jsonplaceholder.typicode.com/posts/${posts[i].id}" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>`;
            $('#all-posts').append(post);
        }
    }

    function getAllPostsFromApi() {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/posts',
            type: 'GET',
            success: function (responseBody) {
                printAllPosts(responseBody);
            }
        });
    }

    // Events
    getAllPostsFromApi();
});
