"use strict";
//
// const btnHid = document.getElementById('btn-hid-divs'),
//     divs = document.getElementsByTagName('div')
// ;
//
//
// // functions (Actions)
// const btnHidHandle = function (e) {
//     for (let i = 0; i < divs.length; i++) {
//         if (divs[i].classList.contains('hid')) {
//             divs[i].classList.remove('hid');
//             continue;
//         }
//
//         divs[i].classList.add('hid');
//     }
// };
//
//
// // Events
// btnHid.addEventListener('click', btnHidHandle);

// JQuery
$('#btn-hid-divs').click(function () {
    $('div').toggleClass('hid').fadeToggle()
});

// const $ = function (query) {
//     if (document.querySelectorAll(query).length === 1) {
//         return {
//             show: function () {
//                 document.querySelectorAll(query)[0].style.display = 'block';
//             },
//             hide: function () {
//                 document.querySelectorAll(query)[0].style.display = 'none';
//
//             }
//         };
//     }
//     const elms = document.querySelectorAll(query);
//
//     return {
//         show: function () {
//             for(let i = 0; i < elms.length; i++) {
//                 elms[i].style.display = 'block';
//             }
//
//             console.log('show');
//
//             return this;
//         },
//         hide: function () {
//             for(let i = 0; i < elms.length; i++) {
//                 elms[i].style.display = 'none';
//             }
//
//             console.log('hide');
//
//             return this;
//         },
//         red: function () {
//             for(let i = 0; i < elms.length; i++) {
//                 elms[i].style.backgroundColor = 'red';
//             }
//
//             console.log('red');
//
//             return this;
//         }
//     };
// };
//
//
// console.log($('div').show().red().hide());
