import React from 'react';

export const TaskForm = props => {
  return (
    <form className="form" onSubmit={props.addNewTaskOnSubmit}>
      <div className="form-group">
        <label htmlFor="title">Title</label>
        <input type="text"
               className="form-control"
               id="newTaskTitle"
               value={props.newTaskTitle}
               onChange={props.newTaskTitleOnChange}/>
      </div>
      <div className="form-group">
        <label htmlFor="title">Sub-Tasks:
          <button className="btn btn-info m-2"
                  type="button"
                  onClick={props.addSubTaskOnClickHandle}>+</button>
        </label>
        {props.subTaskInputs.map((input, index) => (
          <div className="row mb-3" key={input.id}>
            <input type="text"
                   data-index={index}
                   value={input.title}
                   className="col-10 form-control sub-tasks m-1"
                   onChange={props.subTaskInputOnChange}/>
            {props.subTaskInputs.length > 1 && <button type="button"
                                                            data-id={input.id}
                                                            className="col btn btn-danger"
                                                            onClick={props.removeSubTaskOnClickHandle}>-</button>}
          </div>
        ))}

      </div>
      <div className="text-center">
        <button className="btn btn-success">Save</button>
      </div>
    </form>
  )
};