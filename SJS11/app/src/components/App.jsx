import React, {Component} from 'react';
import Layouts from '../layouts';
import Tasks from './Tasks';
import AddNewTask from "../pages/Tasks/AddNewTask";
import {Route, Switch} from "react-router-dom";
import TaskShow from "../pages/Tasks/TaskShow";
// import {TaskList} from "./Tasks/List";
// import TaskList from './Tasks/List';

class App extends Component {

  state = {
    newTaskTitle: '',
    subTaskInputs: [
      {
        id: 1,
        title: ''
      }
    ],
    tasks: [
      {
        id: 1,
        title: 'Task one',
        isDone: false,
        subTasks: [
          {
            id: 1,
            title: 'Test',
            isDone: false
          },
          {
            id: 2,
            title: 'Test 2',
            isDone: true
          }
        ]
      },
      {
        id: 2,
        title: 'Task two',
        isDone: true,
        subTasks: [
          {
            id: 1,
            title: 'Task two Test',
            isDone: true
          },
          {
            id: 2,
            title: 'Task two Test 2',
            isDone: true
          }
        ]
      }
    ]
  };

  addSubTaskOnClickHandle = () => {
    const newInput = {
      id: Date.now(),
      title: ''
    };

    // ES6
    const subTaskInputs = [...this.state.subTaskInputs, newInput];
    // ES5
    // const subTaskInputs = this.state.subTaskInputs;
    // subTaskInputs.push(newInput);

    this.setState({subTaskInputs});
  };

  removeSubTaskOnClickHandle = e => {
    const rowId = e.target.dataset.id;// 2

    const subTaskInputs = this.state.subTaskInputs.filter(input => rowId != input.id); // condition => false);

    /*const arr = [];// temp array
    for (let i = 0; i < this.state.subTaskInputs.length; i++) {
      const input = this.state.subTaskInputs[i];

      if (rowId != input.id) {
        arr.push(input);
      }
    }*/


    this.setState({subTaskInputs});
  };

  newTaskTitleOnChange = e => {
    this.setState({[e.target.id]: e.target.value});
  };

  subTaskInputOnChange = e => {
    const subTaskInputs = this.state.subTaskInputs;
    subTaskInputs[e.target.dataset.index].title = e.target.value;
    this.setState({subTaskInputs});
  };

  addNewTaskOnSubmit = e => {
    e.preventDefault();
    const newTask = {
        id: Date.now(),
        title: this.state.newTaskTitle,
        subTasks: this.state.subTaskInputs
      },
      tasks = [...this.state.tasks, newTask];

    this.setState({tasks});
    this.resetForm();
  };

  resetForm = () => {
    const newTaskTitle = '',
      subTaskInputs = [
        {
          id: 1,
          title: ''
        }
      ];

    this.setState({newTaskTitle, subTaskInputs})
  };

  findTask = taskId => {
    const results = this.state.tasks.filter(task => taskId == task.id);
    if (results.length > 0) {
      return results[0];
    }
    return null;
  };

  render() {
    return (
      <div>
        <Layouts.Nav/>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-10">
              <Switch>
                <Route path="/user/profile"
                       component={(props) => {
                         console.log(props);
                         return <h1>User Profile</h1>;
                       }}/>



                <Route path="/task/create"
                       render={props => <AddNewTask {...props}
                                                       newTaskTitle={this.state.newTaskTitle}
                                                       newTaskTitleOnChange={this.newTaskTitleOnChange}
                                                       addSubTaskOnClickHandle={this.addSubTaskOnClickHandle}
                                                       subTaskInputs={this.state.subTaskInputs}
                                                       subTaskInputOnChange={this.subTaskInputOnChange}
                                                       removeSubTaskOnClickHandle={this.removeSubTaskOnClickHandle}
                                                       addNewTaskOnSubmit={this.addNewTaskOnSubmit}/>}/>

                <Route path="/task/:id" exact
                       render={props => (<TaskShow {...props} findTask={this.findTask}/>)}/>

                <Route path="/" exact>
                  <div className="content mt-3 mb-3">
                    <Tasks.List tasks={this.state.tasks}/>
                  </div>
                </Route>

                <Route path="*">
                  <h1>404 Page Not found</h1>
                </Route>
              </Switch>
            </div>
          </div>
        </div>
        <Layouts.Footer/>
      </div>
    );
  }
}

export default App;