import React from 'react';
import {NavLink} from "react-router-dom";

export const Nav = () => (
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <a className="navbar-brand" href="#">LOGO</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>

    <div className="collapse navbar-collapse">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <NavLink className="nav-link" to="/" exact>All Tasks</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/task/create" exact>Create New Task</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/user/profile" exact>User</NavLink>
        </li>
      </ul>
    </div>
  </nav>
);