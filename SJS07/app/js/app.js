"use strict";
// define vars
const tasks = [
    {
        id: 1,
        title: 'Task one',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        isDone: false,
        createdAt: '14-02-2020'
    },
    {
        id: 2,
        title: 'Task two',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        isDone: true,
        createdAt: '19-02-2020'
    },
    {
        id: 3,
        title: 'Task three',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        isDone: true,
        createdAt: '19-02-2020'
    }
];

// Actions (functions)
function printTasks() {
    $('#tasks').empty();
    for (let i = 0; i < tasks.length; i++) {
        $('#tasks').append(`
            <li class="list-group-item d-flex justify-content-between">
                <div class="content">
                    <h3 class="task-title ${tasks[i].isDone? 'line-through' : ''}" data-body="#task-body-${i}">
                        ${tasks[i].title} | <span class="text-info h-6">${tasks[i].createdAt}</span>
                    </h3>
                    <hr />
                    <p id="task-body-${i}" class="task-body d-none">
                        ${tasks[i].body}
                    </p>
                </div>

                <div class="actions">
                    <button class="btn btn-info ${tasks[i].isDone? 'd-none' : ''}" data-task-id="${tasks[i].id}">
                        Edit
                    </button>
                    <button class="toggle-done btn btn-${tasks[i].isDone?  'danger' : 'success'}" data-task-id="${tasks[i].id}">
                        ${ tasks[i].isDone? 'Re-Open' : 'Done' }
                    </button>
                </div>
            </li>
        `);
    }
}

function restForm() {
    $('input#title').val('');
    $('textarea#body').val('');
}

const addTaskHandle = function () {
    const newTask = {
        id: 9,
        title: $('input#title').val(),
        body: $('textarea#body').val(),
        isDone: false,
        createdAt: '13-06-2020'
    };

    tasks.push(newTask);
    restForm();
    printTasks();
};

function toggleDoneHandle () {
    const targetItem = $(this).data('task-id');

    for (let i = 0; i < tasks.length; i++) {
        if (targetItem == tasks[i].id) {
            tasks[i].isDone = !tasks[i].isDone;
            break;
        }
    }

    printTasks();
}

// events
$(function () { // page ready
    printTasks();

    $('button#btn-add-task').click(addTaskHandle);

    // $('h3.task-title').click(function (e) {
    //     // $($(this).data('body')).toggleClass('d-none');
    //     $(this).nextAll('p.task-body').toggleClass('d-none');
    // });

    // $('.toggle-done').click(function (e) {
    //     console.log('Clicked', $(this).data('task-id'));
    //     const targetItem = $(this).data('task-id');
    //
    //     for (let i = 0; i < tasks.length; i++) {
    //         if (targetItem == tasks[i].id) {
    //             tasks[i].isDone = !tasks[i].isDone;
    //             break;
    //         }
    //     }
    //
    //     printTasks();
    // });
});


// event-delegation
$(document).on('click', '.toggle-done', toggleDoneHandle);

$(document).on('click', 'h3.task-title', function () {
    $(this).nextAll('p.task-body').toggleClass('d-none');
});


// document.addEventListener('click', function (e) {
//     if (e.target.classList.contains('toggle-done')) {
//         console.log('clicked')
//     }
// });

//
// document.addEventListener('DOMContentLoaded', function () {
//     printTasks();
//
//     $('button#btn-add-task').click(addTaskHandle);
// })
