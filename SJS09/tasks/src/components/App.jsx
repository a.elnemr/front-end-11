import React, {Component, Fragment} from "react";
import '../assets/scss/app.scss';
import Nav from "../layouts/Nav";
import TaskItem from "./tasks/TaskItem";

class App extends Component { // React.Component

    state = {
        tasks: [
           {
               id: 1,
               title: 'Tasks Title',
               description: 'Description',
               isDone: true,
               createdAt: '18-07-2020'
           },
           {
               id: 2,
               title: 'Tasks Title 2',
               description: 'Description 2',
               isDone: false,
               createdAt: '19-07-2020'
           },
           {
               id: 3,
               title: 'Tasks Title 3',
               description: 'Description 3',
               isDone: true,
               createdAt: '20-07-2020',
           }
       ],
        newTask: {
            title: '',
            description: '',
            isDone: false,
            createdAt: '18-09-202'
        },
        isAddNewTask: false
    };

    /*constructor(props) {
        super(props);
        // this.isAddNewTaskHandle = this.isAddNewTaskHandle.bind(this);
    }*/

    // isAddNewTaskHandle () {
    //     console.log(this);
    //     // this.state.isAddNewTask = true;
    // }

    isAddNewTaskHandle = () => {
        this.setState({
            isAddNewTask: !this.state.isAddNewTask
        });
    };

    /*titleOnChaneHandle = (e) => {
        this.setState({
            newTask: {
                title: e.target.value
            }
        })
    };*/

    onChaneHandle = e => {
        const newTask = this.state.newTask;

        newTask[e.target.id] = e.target.value;

        this.setState({newTask});
    };

    onSubmitHandle = e => {
        e.preventDefault();
        // const tasks = this.state.tasks;
        // ES 5
        // tasks.push(this.state.newTask);

        // ES 6
        const tasks = [...this.state.tasks, this.state.newTask];

        this.setState({tasks});
        this.isAddNewTaskHandle();
    };


    render() {
        // console.log(this)
        // const tasks = this.state.tasks;
        const {tasks} = this.state;
        return (
            <Fragment>
                <Nav/>
                <div className="container">
                    <h1>Tasks</h1>
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <button className={`btn btn-success ${this.state.isAddNewTask? 'd-none' : ''}`}
                                    onClick={this.isAddNewTaskHandle}>
                                Add New
                            </button>

                            <div className={`form w-100 ${this.state.isAddNewTask? '' : 'd-none'}`}>
                                <form onSubmit={this.onSubmitHandle}>
                                    <div className="form-group">
                                        <label htmlFor="title">Title</label>
                                        <input type="text"
                                               id="title"
                                               className="form-control"
                                               value={this.state.newTask.title}
                                               placeholder="Title"
                                               onChange={this.onChaneHandle}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="description">Description</label>
                                        <textarea id="description" cols="30"
                                                  rows="10"
                                                  className="form-control"
                                                  onChange={this.onChaneHandle}
                                                  value={this.state.newTask.description}/>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-success m-2">Save</button>
                                        <button type="button" className="btn btn-warning m-2" onClick={this.isAddNewTaskHandle}>Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        {/*{this.state.tasks.map(function (task) {
                            return (
                                <TaskItem key={task.id} task={task}/>// props
                            );
                        })}*/}
                        {tasks.map(task => <TaskItem key={task.id} task={task} />)}
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default App;
