import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
// import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/scss/index.scss';


ReactDOM.render(
    <App />,
  document.getElementById('root')
);
