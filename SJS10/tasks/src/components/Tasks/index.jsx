import {TaskList} from './List';
import {TaskItem} from "./Item";
import {TaskForm} from "./Form";

export default {
  List: TaskList,
  Item: TaskItem,
  Form: TaskForm
};