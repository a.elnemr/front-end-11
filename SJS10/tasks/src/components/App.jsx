import React, {Component} from 'react';
import Layouts from '../layouts';
import Tasks from './Tasks';

class App extends Component{

  state = {
    newTaskTitle: '',
    subTaskInputs: [
      {
        id: 1,
        title: ''
      }
    ],
    tasks: [
      {
        id: 1,
        title: 'Task one',
        isDone: false,
        subTasks: [
          {
            id: 1,
            title: 'Test',
            isDone: false
          },
          {
            id: 2,
            title: 'Test 2',
            isDone: true
          }
        ]
      },
      {
        id: 2,
        title: 'Task two',
        isDone: true,
        subTasks: [
          {
            id: 1,
            title: 'Task two Test',
            isDone: true
          },
          {
            id: 2,
            title: 'Task two Test 2',
            isDone: true
          }
        ]
      }
    ]
  };

  addSubTaskOnClickHandle = () => {
    const newInput = {
      id: Date.now(),
      title: ''
    };

    // ES6
    const subTaskInputs = [...this.state.subTaskInputs, newInput];
    // ES5
    // const subTaskInputs = this.state.subTaskInputs;
    // subTaskInputs.push(newInput);

    this.setState({subTaskInputs});
  };

  removeSubTaskOnClickHandle = e => {
    const rowId = e.target.dataset.id;

    const subTaskInputs = this.state.subTaskInputs.filter(input => {
      return rowId != input.id; // condition => false
    });

    this.setState({subTaskInputs});
  };

  newTaskTitleOnChange = e => {
    this.setState({[e.target.id]: e.target.value});
  };

  subTaskInputOnChange = e => {
    const subTaskInputs = this.state.subTaskInputs;
    subTaskInputs[e.target.dataset.index].title = e.target.value;
    this.setState({subTaskInputs});
  };

  addNewTaskOnSubmit = e => {
    e.preventDefault();
    const newTask = {
      id: Date.now(),
      title: this.state.newTaskTitle,
      subTasks: this.state.subTaskInputs
    },
      tasks = [...this.state.tasks, newTask];

    this.setState({tasks});

  };

  render() {
    return (
      <div>
        <Layouts.Nav/>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-10">
              <div className="new-task">
                <Tasks.Form newTaskTitle={this.state.newTaskTitle}
                            newTaskTitleOnChange={this.newTaskTitleOnChange}
                            addSubTaskOnClickHandle={this.addSubTaskOnClickHandle}
                            subTaskInputs={this.state.subTaskInputs}
                            subTaskInputOnChange={this.subTaskInputOnChange}
                            removeSubTaskOnClickHandle={this.removeSubTaskOnClickHandle}
                            addNewTaskOnSubmit={this.addNewTaskOnSubmit}/>
              </div>
              <div className="content mt-3 mb-3">
                <Tasks.List tasks={this.state.tasks}/>
              </div>
            </div>
          </div>
        </div>
        <Layouts.Footer/>
      </div>
    );
  }
}

export default App;